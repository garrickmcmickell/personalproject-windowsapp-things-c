﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Things
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DispatcherTimer timer;
        Random rand = new Random();
        List<thing> things = new List<thing>();
        List<thing> addEm = new List<thing>();
        List<noms> theNoms = new List<noms>();

        public MainWindow()
        {
            InitializeComponent();

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(.05);
            timer.Tick += new EventHandler(timer_Tick);

            for (int i = 0; i < 10000; i++)
            {
                noms aNoms = new noms(rand.Next(101) * 5, rand.Next(106) * 5, this);
                theNoms.Add(aNoms);
            }

            for (int i = 0; i < 1; i++)
            {
                thing aThing = new thing(250, 250, this);
                aThing.theNoms = this.theNoms;
                things.Add(aThing);
                aThing.theThings = this.things;              
            } 

            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {

            for (int i = 0; i < things.Count; i++) {
                things[i].move(rand.Next(-1, 2), rand.Next(-1, 2));
            }
            
            foreach(thing t in addEm)
            {
                things.Add(t);
            }

            addEm = new List<thing>();

        }

        public void addIt(int x, int y)
        {
            thing aThing = new thing(x, y, this);
            aThing.theNoms = this.theNoms;
            addEm.Add(aThing);
            aThing.theThings = this.things;
        }
}
}
