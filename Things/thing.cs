﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Things
{
    class thing
    {
        MainWindow window;
        public List<noms> theNoms { get; set; }
        public List<thing> theThings { get; set; }
        //public List<thing> addIt { get; set; }
        Rectangle body = new Rectangle();
        int x;
        int y;
        int xGrid;
        int yGrid;
        int lvl = 0;
        int currentNoms = 0;       

        public thing(int xStart, int yStart, MainWindow w)
        {
            window = w;
            x = xStart;
            y = yStart;
            xGrid = x / 5;
            yGrid = y / 5;
            body.Width = 5;
            body.Height = 5;
            body.Stroke = new SolidColorBrush(Colors.Lime);
            window.asdf.Children.Add(body);
            Canvas.SetTop(body, xStart);
            Canvas.SetLeft(body, yStart);
        }

        public void move(int xRand, int yRand)
        {
            
            if (xGrid > 0 && xGrid < 106)
            {
                x += 5 * xRand;
                xGrid = x / 5;
            }
            else if(xGrid == 0)
            {
                if (xRand >= 0)
                {
                    x += 5 * xRand;
                    xGrid = x / 5;
                }
            }
            else
            {
                if (xRand <= 0)
                {
                    x += 5 * xRand;
                    xGrid = x / 5;
                }
            }

            if (yGrid > 0 && yGrid < 101)
            {
                y += 5 * yRand;
                yGrid = y / 5;
            }
            else if (yGrid == 0)
            {
                if (yRand >= 0)
                {
                    y += 5 * yRand;
                    yGrid = y / 5;
                }
            }
            else
            {
                if (yRand <= 0)
                {
                    y += 5 * yRand;
                    yGrid = y / 5;
                }
            }

            Canvas.SetTop(body, y);
            Canvas.SetLeft(body, x);

            eatNoms();
            eatThings();
            checkLvl();
        }

        public void eatNoms()
        {
            noms eatThis = null;

            foreach (noms n in theNoms)
            {
                if (xGrid == n.xGrid && yGrid == n.yGrid && this.lvl < 3)
                {
                    eatThis = n;
                    currentNoms++;
                }
            }

            if (eatThis != null)
            {
                theNoms.Remove(eatThis);
                window.asdf.Children.Remove(eatThis.body);
            }
        }

        public void eatThings() {
            thing eatThis = null;

            foreach (thing t in theThings) {
                if (xGrid == t.xGrid && yGrid == t.yGrid && this.lvl > t.lvl) {
                    eatThis = t;
                    body.Stroke = new SolidColorBrush(Colors.Red);
                    lvl = 3;
                }
            }

            if (eatThis != null) {
                theThings.Remove(eatThis);
                window.asdf.Children.Remove(eatThis.body);
            }
        }

        public void checkLvl()
        {
            if(lvl == 0 && currentNoms >= 5)
            {
                body.Stroke = new SolidColorBrush(Colors.Cyan);
                lvl++;
                currentNoms = 0;             
            }

            if(lvl == 1 && currentNoms >= 5)
            {
                split();
                currentNoms = 0;
            }
        }

        public void split()
        {
            body.Stroke = new SolidColorBrush(Colors.Lime);
            lvl = 0;

            window.addIt(x, y);
        }

    }
}
